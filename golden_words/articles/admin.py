from django.contrib import admin

# Register your models here.
from articles.models import Article, Category, \
    Image, Video, Interesting, CallbackForm
from image_cropping import ImageCroppingMixin


#
#
# @admin.register(Article)
# class ArticleAdmin(admin.ModelAdmin):
#     prepopulated_fields = {"slug": ("title",)}


@admin.register(Interesting)
class InterestingAdmin(admin.ModelAdmin):
    pass


@admin.register(CallbackForm)
class CallbackFormAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class ImageInline(admin.StackedInline):
    model = Image


class VideoInline(admin.StackedInline):
    model = Video


@admin.register(Article)
class RoomAdmin(ImageCroppingMixin, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    inlines = [ImageInline, VideoInline]
    search_fields = ['title']

    # def get_search_results(self, request, queryset, search_term):
    #     try:
    #         search_term_as_int = int(search_term)
    #     except ValueError:
    #         pass
    #     else:
    #         queryset |= self.model.objects.filter(title__icontains=search_term_as_int)
    #         print('queryset', queryset)
    #     return queryset, True
