from django import template
from django.contrib.auth.forms import UserCreationForm
from articles.models import Category
import requests
from googletrans import Translator

register = template.Library()


@register.filter
def get_id(link):
    return link[link.rfind('/') + 1:]


@register.simple_tag()
def get_categories():
    all_categories = Category.objects.all()
    return {
        'all_categories': all_categories
    }


IP_URL = "http://api.hostip.info/?ip="

import urllib.request as urllib2


def get_coords(ip):
    url = IP_URL + ip
    respond = None
    try:
        respond = urllib2.urlopen(url).read()
    except Exception as e:
        print(type(e), e)
        return

    if respond:
        pass
        # Parse de returned XML Here, you can use MiniDom


@register.simple_tag()
def get_weather(req):
    x_forwarded_for = req.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = req.META.get('REMOTE_ADDR')
    print('ip', get_coords(ip))
    url = 'http://api.openweathermap.org/data/2.5/find?q=Kyiv&type=like&APPID=426c70ac3968caec9bbe97ce1ee52f17'
    s_city = "Kyiv"
    appid = '426c70ac3968caec9bbe97ce1ee52f17'
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/find",
                           params={'q': s_city, 'type': 'like', 'units': 'metric', 'APPID': appid})
        data = res.json()
        cities = ["{} ({})".format(d['name'], d['sys']['country'])
                  for d in data['list']]
        print("city:", cities)
        city_id = data['list'][0]['id']
        print('city_id=', city_id)
    except Exception as e:
        print("Exception (find):", e)
        pass

    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                           params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        from pprint import pprint
        temp = data['main']['temp']
        temp_min = data['main']['temp_min']
        temp_max = data['main']['temp_max']
        icon = data['weather'][0]['icon']
        description = data['weather'][0]['description']
        translator = Translator()
        desc_uk = translator.translate(description, src='en', dest='uk')
        print(desc_uk.text)
        return {
            'temp': temp,
            'temp_min': temp_min,
            'temp_max': temp_max,
            'icon': icon,
            'description': desc_uk.text.capitalize(),
        }
    except Exception as e:
        print("Exception (weather):", e)
        pass



@register.simple_tag()
def privatbank():
    try:
        response = requests.post('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5').json()
        print('response' , response)
        return response
    except Exception as e:
        return None

