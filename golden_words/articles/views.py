from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, render_to_response
from .models import Article, Category, Interesting
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from htmlmin.decorators import minified_response
from django.core.paginator import Paginator
from .models import CallbackForm


# Create your views here.
@minified_response
def home(request):
    ctx = {}
    ctx['articles'] = Article.objects.filter(status=True).order_by('-id')[:31]
    return render(request, 'articles/home.html', ctx)


@minified_response
def article_detail(request, slug):
    ctx = {}
    try:
        article = Article.objects.get(slug=slug)
        read_more_articles = article.articles.filter(status=True) if article.articles.all() \
            else Article.objects.filter(category__pk=article.category.id, status=True).order_by('-id')[:10]
        ctx['article'] = article
        ctx['read_more_articles'] = read_more_articles
        ctx['last_articles'] = Article.objects.filter(status=True).exclude(id=article.id).order_by('-id')
        image = get_image_from_content(article.content)
        if image:
            if 'zoloti.com.ua' not in image:
                ctx['opengraph_img_url'] = 'https://zoloti.com.ua' + image
            else:
                ctx['opengraph_img_url'] = image
        else:
            ctx['opengraph_img_url'] = 'https://zoloti.com.ua' + article.image.url if article.image else ''

    except Exception as e:
        print(type(e), e)
    return render(request, 'articles/article_detail.html', ctx)


def get_image_from_content(content):
    img_tag = '<img'
    index_img_tag = content.find(img_tag)
    if index_img_tag == -1:
        return
    slice_image = content[index_img_tag:]
    src = slice_image[slice_image.find('src="') + len('src="'):]
    clean_url = src[:src.find('"')]
    return clean_url


@minified_response
def category(request, slug):
    ctx = {}
    articles = Article.objects.filter(category__slug=slug, status=True).order_by('-id')
    print('articles ' , articles)
    page = request.GET.get('page', 1)

    paginator = Paginator(articles, 12)
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)
    ctx['articles'] = articles
    # ctx['category'] = Category.objects.get(slug=slug)
    return render(request, 'articles/category.html', ctx)


def load_more(request, from_, to):
    articles = Article.objects.filter(status=True).order_by('-id')[from_:to]
    response_str = render_to_response('load_more.html', {
        'articles': articles
    }).content.decode('utf-8')
    return JsonResponse({
        'template': response_str,
        'end': len(articles) < 31
    })


@minified_response
def interesting(request):
    # print('asdasdasd!!!!!!!!')
    # all_facts = Interesting.objects.all()
    return render(request, 'articles/interesting.html', {
        # 'facts': all_facts
    })


def form(request):
    if request.method == 'POST':
        if request.is_ajax():
            print('AJAX!!! POST')
            email = request.POST.get('email')
            msg = request.POST.get('msg')
            form = CallbackForm()
            form.msg = msg
            form.email = email
            form.save()
            return JsonResponse({
                'status': True
            })


def add_count_articles(request, article_id):
    if request.is_ajax():
        print('article id ', article_id)
        article = Article.objects.get(id=int(article_id))
        article.count += 1
        article.save(add_count=True)
        return JsonResponse({
            'add': True
        })


def api(request):
    return JsonResponse({
        'status': True
    })


def parser_get_article_by_category(request , category):
    all_articles  = Article.objects.filter(category__slug=category , status=True)
    response_article = [{
        'title': art.title,
        'meta_desc': art.meta_desc,
        'meta_keywords': art.meta_keywords,
        'content': art.content
    }
                        for art in all_articles]
    print('all_articles' , all_articles)
    return JsonResponse({
        'status': True,
        'articles': response_article
    })