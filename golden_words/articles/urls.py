"""golden_words URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views

app_name = 'articles'
urlpatterns = [
    path('', views.home, name='home'),
    path('<slug:slug>/', views.article_detail, name='article'),
    path('category/<slug:slug>/', views.category, name='category'),
    path('js_load_more/<int:from_>/<int:to>/', views.load_more),
    path('statistic/covid-19/', views.interesting, name='inetresting'),
    path('form/callback_form_ajax/', views.form, name='form'),
    path('api/articles/', views.api, name='api'),
    path('add_count_articles/<int:article_id>/', views.add_count_articles, name='add_count_articles'),
    path('parser_get_article_by_category/<str:category>/' , views.parser_get_article_by_category)

]
