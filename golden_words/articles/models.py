from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.shortcuts import redirect
from pytils.translit import slugify
from mptt.models import MPTTModel, TreeForeignKey
from sortedm2m.fields import SortedManyToManyField
from PIL import Image as pil_imag

from image_cropping import ImageRatioField
# Create your models here.

#from image_optimizer.fields import OptimizedImageField


class Category(MPTTModel):
    name = models.CharField(max_length=255, verbose_name='Название категории', unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=300, unique=False, blank=True, null=True)
    meta_desc = models.TextField(max_length=1000, verbose_name='мета дескрипшин', default='', blank=True, null=True)
    meta_keywords = models.TextField(max_length=1000, verbose_name='мета ключові слова', default='', blank=True,
                                     null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return '{} {}'.format(self.name, self.status)


class Article(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заголовок статті', unique=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name='категория статьи')
    image = models.ImageField(upload_to='article_image/',
                              default='', blank=True,
                              null=True, verbose_name='Головне фото')
    cropping_25 = ImageRatioField('image', '273x200', size_warning=True, verbose_name='Размер для колонки 1/4')
    cropping_33 = ImageRatioField('image', '175x151', size_warning=True, verbose_name='Размер для колонки 1/3')
    cropping_50 = ImageRatioField('image', '240x150', size_warning=True, verbose_name='Размер для колонки 1/2')
    content = RichTextUploadingField(verbose_name='Контент')
    status = models.BooleanField(default=True)
    articles = SortedManyToManyField('self', blank=True, null=True)
    slug = models.SlugField(max_length=300, unique=False, blank=True, null=True)

    meta_desc = models.TextField(max_length=1000, verbose_name='мета дескрипшин', default='', blank=True, null=True)
    meta_keywords = models.TextField(max_length=1000, verbose_name='мета ключові слова', default='', blank=True,
                                     null=True)

    social_image = models.ImageField(upload_to='social_image/', blank=True, null=True, default='')
    show_count = models.BooleanField(default=True, verbose_name='Показувати лічильник')
    count = models.IntegerField(default=743)
    date = models.DateTimeField(null=True, blank=True)

    def get_videos(self):
        return Video.objects.filter(article=self)

    def save(self, *args, **kwargs):
        try:
            if kwargs.pop('add_count'):
                kwargs.pop('add_count')
                print('add_count')
                # return
            else:
                self.slug = slugify(self.title)
                print(slugify)
        except KeyError as err:
            self.slug = slugify(self.title)
            print(slugify)
        super(Article, self).save(*args, **kwargs)
        # except AttributeError as e:
        #     print('AttributeError')
        #     self.slug = slugify(self.title)
        #     print(slugify)
        #
        #     instance = super(Article, self).save(*args, **kwargs)
        #     if self.image:
        #         image = pil_imag.open(self.image.path)
        #         image.save(self.image.path, quality=30, optimize=True)
        #     return instance
        # except KeyError as e:
        #     print('KeyError')
        #     self.slug = slugify(self.title)
        #     print(slugify)
        #     instance = super(Article, self).save(*args, **kwargs)
        #     if self.image:
        #         print('self ', self.image)
        #         image = pil_imag.open(self.image.path)
        #         image.save(self.image.path, quality=30, optimize=True)
        #         return self

    # reverse('people.views.details', args=[str(self.id)])
    def get_absolute_url(self):
        from django.urls import reverse
        return "https://zoloti.com.ua/%s/" % self.slug

    def __str__(self):
        return '{} {}'.format(self.title, self.content[:50] + '... ' if len(self.content) > 80 else self.content)


class Image(models.Model):
    img = models.ImageField(upload_to='images_article', blank=True
                            , null=True, default='')
    article = models.ForeignKey(Article, related_name='article', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.img, self.article.title)


class Video(models.Model):
    CHOICE_TYPE = (
        ('youtube', 'C ютуба'),
        ('download', 'C компьютера')
    )
    video = models.FileField(upload_to='video_images/', blank=True
                             , null=True, default='')
    article = models.ForeignKey(Article, related_name='article_video', on_delete=models.CASCADE, blank=True, null=True)
    yotube_id = models.CharField(max_length=1500, verbose_name='Ccылка на ютуу видео', blank=True, null=True,
                                 default='')

    choices = models.CharField(max_length=30, verbose_name='Источник',
                               choices=CHOICE_TYPE,
                               default='youtube')

    def __str__(self):
        return '{} {}'.format(self.video, self.article.title)


class Interesting(models.Model):
    content = RichTextUploadingField()
    t = models.CharField(max_length=2 , blank=True, null=True, default ='')
    def __str__(self):
        return '{}'.format(self.content[:20])

    class Meta:
        verbose_name = 'цікавий факт'
        verbose_name_plural = 'Цікаві факти'


class CallbackForm(models.Model):
    email = models.CharField(max_length=150, verbose_name='E-mail', blank=True, null=True)
    msg = models.TextField(max_length=2000, verbose_name='Cообщение от пользователя', blank=True, null=True, default='')

    def __str__(self):
        return '{} {}'.format(self.email, self.msg)
