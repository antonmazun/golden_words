MIDDLEWARE_CLASSES = (
    # other middleware classes
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
)
HTML_MINIFY = True

KEEP_COMMENTS_ON_MINIFYING = True
from easy_thumbnails.conf import Settings as thumbnail_settings

THUMBNAIL_PROCESSORS = (
                           'image_cropping.thumbnail_processors.crop_corners',
                       ) + thumbnail_settings.THUMBNAIL_PROCESSORS

IMAGE_CROPPING_SIZE_WARNING = True
# To use Pillow
OPTIMIZED_IMAGE_METHOD = 'pillow'

from .settings import INSTALLED_APPS

INSTALLED_APPS.append('easy_thumbnails')
INSTALLED_APPS.append('image_cropping')
