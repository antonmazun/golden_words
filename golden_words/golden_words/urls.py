"""golden_words URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, reverse
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import GenericSitemap
from articles.models import Article

from django.contrib.sitemaps import Sitemap

info_dict = {
    'queryset': Article.objects.filter(status=True).order_by('-id'),
    'date_field': 'date',
}


class ArticleSitemap(Sitemap):
    changefreq = "daily"
    date_field = 'date'
    priority = 0.5

    def items(self):
        return Article.objects.filter(status=True).order_by('-id')

    def location(self, item):
        print(item)
        return reverse('articles:article', kwargs={
            'slug': item.slug
        })

    def lastmod(self, item):
        return item.date


urlpatterns = [

    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('admin/', admin.site.urls),
    path('', include('articles.urls', namespace='articles')),
    path('sitemap.xml', sitemap, {'sitemaps': {'blog': ArticleSitemap}},
         name='django.contrib.sitemaps.views.sitemap')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_ROOT, document_root=settings.STATIC_URL)
urlpatterns += [
]
