import Home from './js/home';
import $ from 'jquery';
import VideoPlayer from './js/video_player';
import Stats from './js/stats';

// import lazyLoadXT from 'lazyloadxt/dist/jqlight.lazyloadxt';

class Root {
    constructor() {
        this.home = new Home();
        this.video_player = new VideoPlayer();
        this.stats = new Stats();

    }


    init() {
        // $('img').lazyLoadXT();
    }


    lazy_laod() {
        $(document).ready(function () {
            (function ($) {
                $('.lazy').lazyload({
                    data_attribute: "src"
                });
            }($));
        });
    }

    mobile_header() {
        $('.js-mob-menu').click(function () {
            $(this).toggleClass('open');
            $('.mobile-wrap').toggleClass('menu-open');
            $('.logo_link').toggleClass('abs-logo');
            $('.logo-txt').toggleClass('white-color');

            if ($('.js-mob-menu ').hasClass('open')) {
            }
        })
    }

    drop_right() {
        $('.js-drop-right-1').hover(function () {
            $(this).closest('.js-right').find('.drop-right-wrap').toggleClass('visible');
            console.log(this);
        })
    }


    count_articles(article_id) {
        let url_count = `/add_count_articles/${article_id}/`;
        if (localStorage.getItem(`article_id_${article_id}`) != article_id) {
            // alert('added!!!');
            $.ajax({
                method: 'GET',
                url: url_count
            }).done(function (response) {
                if (response.add) {
                    localStorage.setItem(`article_id_${article_id}`, article_id);
                }
                console.log(response);
            })
            console.log('count article!!!!', url_count);
        } else {
            // alert('not added!')
        }

    }
}

var App = new Root();
window.App = App;

window.$ = $;
