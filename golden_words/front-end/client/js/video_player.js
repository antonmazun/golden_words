import Plyr from 'plyr';

export default class VideoPlayer {
    constructor() {

    }

    init() {
        console.log('VideoPlayer init');
        let youtube_video = $('.js-youtube-video');
        $(youtube_video).each(function (index, elem) {
            console.log('elem ', elem.id);
            var player = new Plyr('#' + elem.id);
        });
    }
}