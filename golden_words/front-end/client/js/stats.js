export default class Stats {
    constructor() {
        this.global_info_url = 'https://coronavirus-19-api.herokuapp.com/all';
        this.global_country = 'https://corona.lmao.ninja/countries';
        this.global_info = {};
    }

    init() {
        console.log('stats init');
        this.get_global();
        let day = new Date().getDate();
        let month = new Date().getMonth();
        let year = new Date().getFullYear();
        let date = `${day}/${month + 1}/${year}`;
        $('.js-now-date').html(date);
    }


    get_global() {
        let component = this;

        $.ajax({
            method: 'GET',
            url: component.global_info_url
        }).done((response) => {
            for (let elem in response) {
                $(`.js-global-${elem}`).text(response[elem]);
            }
        });


        $.ajax({
            method: 'GET',
            url: component.global_country
        }).done((response) => {
            let info_template = '';
            let today_cases = 0;
            let today_letal = 0;
            let our_country_info = ''
            response.map((element, index) => {
                if (element.country === 'Ukraine') {
                    our_country_info += ` <div class="table-row">
                    <div class="table-data " style="font-weight: bold;">${element.country}</div>
                    <div class="table-data ">${element.cases}</div>
                    <div class="table-data " style="background: #f4a694;">${element.todayCases}</div>
                    <div class="table-data " >${element.deaths}</div>
                    <div class="table-data " style="background: #fe2600;color: white; ">${element.todayDeaths}</div>
                    <div class="table-data " >${element.recovered}</div>
                    <div class="table-data ">${element.active}</div>
                    <div class="table-data ">${element.critical}</div>
                </div>`;
                    $('#info_our_country').html(our_country_info);
                } else {
                    info_template += ` <div class="table-row">
                    <div class="table-data " style="font-weight: bold;">${element.country}</div>
                    <div class="table-data ">${element.cases}</div>
                    <div class="table-data " style="background: #f4a694;">${element.todayCases}</div>
                    <div class="table-data ">${element.deaths}</div>
                    <div class="table-data " style="background: #fe2600; color: white;">${element.todayDeaths}</div>
                    <div class="table-data " style="background: #0db194 color: white;" >${element.recovered}</div>
                    <div class="table-data ">${element.active}</div>
                    <div class="table-data ">${element.critical}</div>
                </div>`;
                }
                today_cases += element.todayCases;
                today_letal += element.todayDeaths;

            });
            console.log('today_cases ', today_cases);
            console.log('today_letal ', today_letal);
            $('.js-today-cases').html(today_cases);
            $('.js-today-letal').html(today_letal);
            $('#info_country').html(info_template);
        })

    }


}