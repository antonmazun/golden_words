import $ from 'jquery';
import AOS from 'aos';
// import Lazy from 'jquery-lazy/jquery.lazy'

export default class Home {
    constructor() {
        this.load_more_btn = $('#js_load_more');
        this.OFFSET = 31;
    }

    // lazy_load() {
    //     $(function () {
    //         $('.js-lazy-img').Lazy();
    //     });
    // }

    load_more(btn) {
        let self = this;
        btn.on('click', function (e) {
            let url = '/js_load_more/';
            let from_ = +$('#count_articles').val();
            let to = +from_ + self.OFFSET;
            url += from_ + '/' + to + '/';
            $.ajax({
                url: url,
                method: 'GET'
            }).done(function (response) {
                $('.js-wrap').append(response.template);
                let new_len = $('.js-article').length;
                $('#count_articles').val(new_len)
                if (response.end) {
                    self.load_more_btn.hide();
                }
            })

        })
    }

    get_icon_weather() {
        let icon_code = $('.weather-icon-js').attr('data-icon');

    }

    mobile_header() {
        $('.js-mobile-drop').click(function (e) {
            // e.preventDefault();
            $('.js-mobile-drop').find('.children').removeClass('show');
            $(this).find('.children').toggleClass('show');
            $('.js-mobile-drop').removeClass('arrow-up');
            $(this).toggleClass('arrow-up');

            console.log(e, 'clicked!')
        })

        $('.js-show-menu').click(function (e) {
            console.log(e);
            $(this).toggleClass('open');
            $('.mobile-children').toggleClass('show');
        })
    }

    callback_form() {
        $('.js-btn-form').click(function (e) {
            console.log(e);
            $('.popup-wrap').toggleClass(
                'hide'
            );

        });

        $('#callback_form').on('submit', function (e) {

            e.preventDefault();
            let form = this;
            let csrf_token = $(form).find('input[name="csrfmiddlewaretoken"]').val();
            let email = $(form).find('input[name="email"]').val();
            let msg = $(form).find('textarea[name="msg"]').val();
            $.ajax({
                method: this.method,
                url: this.action,
                headers: {"X-CSRFToken": csrf_token},
                data: {
                    'email': email,
                    'msg': msg,
                }
            }).done(function (response) {
                if (response.status) {
                    $('.popup-wrap').addClass('hide');
                    $('.js-thx-popup').removeClass('hide');
                    setTimeout(() => {
                        $('.js-thx-popup').addClass('hide');
                    }, 3000);

                }
            });
        });

        $('#close_thx').click(function (e) {
            $('.js-thx-popup').addClass('hide');
        })

        $('.js-close').click(function (e) {
            $('.popup-wrap').addClass('hide');
        })
    }

    init() {
        this.load_more(this.load_more_btn);
        // this.callback_form();
    };


}
